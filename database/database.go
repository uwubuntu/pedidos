package database

import (
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"time"
)

type DB struct {
	conn *gorm.DB
}

func Connect() *DB {
	time.Sleep(5 * time.Second)
	dsn := "host=motty.db.elephantsql.com user=yodrxcqw dbname=yodrxcqw password=Z_VgIYczvRX6JLTXj2XF1xQPWD9Ir0nX port=5432 sslmode=disable"
	conn, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Error al conectar a la base de datos MySQL: %v", err)
	}
	return &DB{conn}
}

// Función para obtener la conexión de GORM
func (db *DB) GetConn() *gorm.DB {
	return db.conn
}
