package database

import (
	"log"

	"github.com/Zheol/model"
	"gorm.io/gorm"
)

// EjecutarMigraciones realiza todas las migraciones necesarias en la base de datos.
func EjecutarMigraciones(db *gorm.DB) {

	db.AutoMigrate(&model.OrdenGORM{})
	db.AutoMigrate(&model.ProductoGORM{})
	db.AutoMigrate(&model.RelacionGORM{})
	db.AutoMigrate(&model.UsuarioGORM{})

	log.Println("Migraciones completadas")
}
