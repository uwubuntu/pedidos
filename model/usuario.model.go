package model

import (
	"strconv"
)

// UsuarioGORM es el modelo de usuario para GORM de Usuario
type UsuarioGORM struct {
	ID      uint        `gorm:"primaryKey:autoIncrement" json:"id"`
	Correo  string      `gorm:"type:varchar(255);not null;unique"`
	Ordenes []OrdenGORM `gorm:"foreignKey:UsuarioID;constraint:OnDelete:CASCADE"`
}

// TableName especifica el nombre de la tabla para UsuarioGORM
func (UsuarioGORM) TableName() string {
	return "usuarios"
}

func (usuarioGORM *UsuarioGORM) ToGQL() (*UsuarioPedido, error) {

	var ordenes []*Orden
	for _, ordenGORM := range usuarioGORM.Ordenes {
		// Realiza la conversión de DireccionGORM a Direccion aquí
		orden, _ := ordenGORM.ToGQL()
		ordenes = append(ordenes, orden)
	}

	return &UsuarioPedido{
		ID:      strconv.Itoa(int(usuarioGORM.ID)),
		Correo:  usuarioGORM.Correo,
		Ordenes: ordenes,
	}, nil
}
