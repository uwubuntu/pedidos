package model

type ActualizarProductoInput struct {
	Nombre *string `json:"nombre,omitempty"`
}

type ActualizarRelacionInput struct {
	ProductoID    *string `json:"productoID,omitempty"`
	OrdenID       *string `json:"ordenID,omitempty"`
	Cantidad      *int    `json:"cantidad,omitempty"`
	TotalRelacion *int    `json:"totalRelacion,omitempty"`
	Talla         *string `json:"talla,omitempty"`
	Monto         *int    `json:"monto,omitempty"`
}

type ActualizarUsuarioInput struct {
	Correo *string `json:"correo,omitempty"`
}

type ActualizarOrdenInput struct {
	UsuarioID *string `json:"usuarioID,omitempty"`
	TotalPago *int    `json:"totalPago,omitempty"`
	TokenPago *string `json:"tokenPago,omitempty"`
	Estado    *string `json:"estado,omitempty"`
	Retiro    *string `json:"retiro,omitempty"`
}

type ProductoPedido struct {
	ID     string `json:"id"`
	Nombre string `json:"nombre"`
}

func (ProductoPedido) isEntity() {}

type CrearProductoInput struct {
	Nombre string `json:"nombre"`
}

type RelacionPedido struct {
	ID       string          `json:"id"`
	Producto *ProductoPedido `json:"producto"`
	Orden    *Orden          `json:"orden"`
	Cantidad int             `json:"cantidad"`
	Talla    string          `json:"talla"`
	Monto    int             `json:"monto"`
}

func (RelacionPedido) isEntity() {}

type CrearRelacionInput struct {
	ProductoID string `json:"productoID"`
	OrdenID    string `json:"ordenID"`
	Cantidad   int    `json:"cantidad"`
	Talla      string `json:"talla"`
	Monto      int    `json:"monto"`
}

type Orden struct {
	ID         string            `json:"id"`
	Usuario    *UsuarioPedido    `json:"usuario"`
	TotalPago  int               `json:"totalPago"`
	TokenPago  string            `json:"tokenPago"`
	Estado     string            `json:"estado"`
	Retiro     string            `json:"retiro"`
	Relaciones []*RelacionPedido `json:"relaciones,omitempty"`
}

func (Orden) isEntity() {}

type CrearOrdenInput struct {
	UsuarioID string `json:"usuarioID"`
	TotalPago int    `json:"totalPago"`
	TokenPago string `json:"tokenPago"`
	Estado    string `json:"estado"`
	Retiro    string `json:"retiro"`
}

type UsuarioPedido struct {
	ID      string   `json:"id"`
	Correo  string   `json:"correo"`
	Ordenes []*Orden `json:"ordenes,omitempty"`
}

func (UsuarioPedido) isEntity() {}

type CrearUsuarioInput struct {
	Correo string `json:"correo"`
}

type RespuestaEliminacionPedido struct {
	Mensaje          string `json:"mensaje"`
	CodigoEstadoHTTP int    `json:"codigoEstadoHTTP"`
}
