package model

import (
	"strconv"
)

// UsuarioGORM es el modelo de usuario para GORM de Usuario
type OrdenGORM struct {
	ID         uint           `gorm:"primaryKey:autoIncrement" json:"id"`
	UsuarioID  uint           `gorm:"type:varchar(255);not null"`
	Usuario    UsuarioGORM    `gorm:"foreignKey:UsuarioID"`
	Relaciones []RelacionGORM `gorm:"foreignKey:OrdenID;constraint:OnDelete:CASCADE"`
	TotalPago  int
	TokenPago  string
	Estado     string
	Retiro     string
}

// TableName especifica el nombre de la tabla para UsuarioGORM
func (OrdenGORM) TableName() string {
	return "ordenes"
}

func (ordenGORM *OrdenGORM) ToGQL() (*Orden, error) {

	var relaciones []*RelacionPedido
	for _, relacionGORM := range ordenGORM.Relaciones {
		relacion, _ := relacionGORM.ToGQL()
		relaciones = append(relaciones, relacion)
	}

	ordenUsuario, _ := ordenGORM.Usuario.ToGQL()

	return &Orden{
		ID:         strconv.Itoa(int(ordenGORM.ID)),
		Usuario:    ordenUsuario,
		Relaciones: relaciones,
		TotalPago:  ordenGORM.TotalPago,
		TokenPago:  ordenGORM.TokenPago,
		Estado:     ordenGORM.Estado,
		Retiro:     ordenGORM.Retiro,
	}, nil
}
