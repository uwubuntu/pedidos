package model

import "strconv"

type RelacionGORM struct {
	ID         uint         `gorm:"primaryKey:autoIncrement" json:"id"`
	ProductoID uint         `gorm:"type:varchar(255);not null"`
	Producto   ProductoGORM `gorm:"foreingKey:ProductoID"`
	OrdenID    uint         `gorm:"type:varchar(255);not null"`
	Orden      OrdenGORM    `gorm:"foreingKey:OrdenID"`
	Canidad    int
	Monto      int
	Talla      string
}

func (RelacionGORM) TableName() string {
	return "relaciones"
}

func (relacionGORM RelacionGORM) ToGQL() (*RelacionPedido, error) {
	reProducto, _ := relacionGORM.Producto.ToGQL()
	reOrden, _ := relacionGORM.Orden.ToGQL()

	return &RelacionPedido{
		ID:       strconv.Itoa(int(relacionGORM.ID)),
		Producto: reProducto,
		Orden:    reOrden,
		Cantidad: relacionGORM.Canidad,
		Monto:    relacionGORM.Monto,
		Talla:    relacionGORM.Talla,
	}, nil
}
