package model

import (
	"strconv"
)

type ProductoGORM struct {
	ID     uint `gorm:"primaryKey:autoIncrement" json:"id"`
	Nombre string
}

func (ProductoGORM) TableName() string {
	return "productos"
}

func (productoGORM *ProductoGORM) ToGQL() (*ProductoPedido, error) {
	if productoGORM == nil {
		return nil, nil
	}

	return &ProductoPedido{
		ID:     strconv.Itoa(int(productoGORM.ID)),
		Nombre: productoGORM.Nombre,
	}, nil
}
