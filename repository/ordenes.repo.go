package repository

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/Zheol/database"
	"github.com/Zheol/model"
	"gorm.io/gorm"
)

type OrdenesRepository interface {
	CrearOrden(input model.CrearOrdenInput) (string, error)
	Orden(id string) (string, error)
	ActualizarOrden(id string, input *model.ActualizarOrdenInput) (string, error)
	EliminarOrden(id string) (string, error)
	Ordenes() (string, error)
}

type ordenesRepository struct {
	db *database.DB
}

func NewOrdenesRepository(db *database.DB) OrdenesRepository {
	return &ordenesRepository{db}
}

func (or *ordenesRepository) CrearOrden(input model.CrearOrdenInput) (string, error) {
	var usuario model.UsuarioGORM
	if err := or.db.GetConn().First(&usuario, input.UsuarioID).Error; err != nil {
		return "Error", fmt.Errorf("error al obtener el usuario con ID %s: %v", input.UsuarioID, err)
	}
	ordenGORM := &model.OrdenGORM{
		UsuarioID: usuario.ID,
		TotalPago: input.TotalPago,
		TokenPago: input.TokenPago,
		Estado:    input.Estado,
		Retiro:    input.Retiro,
	}

	result := or.db.GetConn().Create(&ordenGORM)
	if result.Error != nil {
		log.Printf("Error al crear la orden: %v", result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := ordenGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la orden de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (or *ordenesRepository) Orden(id string) (string, error) {
	var ordenGORM model.OrdenGORM
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return "Error", err
	}
	result := or.db.GetConn().Preload("Usuario").Preload("Relaciones.Producto").First(&ordenGORM, idInt)

	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", result.Error
		}
		log.Printf("Error al obtener la orden con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := ordenGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la orden de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (or *ordenesRepository) ActualizarOrden(id string, input *model.ActualizarOrdenInput) (string, error) {
	var ordenGORM model.OrdenGORM
	result := or.db.GetConn().First(&ordenGORM, id)

	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", fmt.Errorf("Orden con ID %s no encontrado", id)
		}
		log.Printf("Error al buscar la orden con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	// Actualizar los campos proporcionados en el input
	if input.TokenPago != nil {
		ordenGORM.TokenPago = *input.TokenPago
	}
	if input.TotalPago != nil {
		ordenGORM.TotalPago = *input.TotalPago
	}
	if input.Estado != nil {
		ordenGORM.Estado = *input.Estado
	}

	if input.Retiro != nil {
		ordenGORM.Retiro = *input.Retiro
	}

	// Actualizar el registro en la base de datos
	result = or.db.GetConn().Save(&ordenGORM)
	if result.Error != nil {
		log.Printf("Error al actualizar la orden con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := ordenGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la orden de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (or *ordenesRepository) EliminarOrden(id string) (string, error) {
	var ordenGORM model.OrdenGORM
	result := or.db.GetConn().First(&ordenGORM, id)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {

			response, _ := &model.RespuestaEliminacionPedido{
				Mensaje:          "La orden no existe",
				CodigoEstadoHTTP: http.StatusNotFound,
			}, result.Error
			return ToJSON(response)

		}
		log.Printf("Error al obtener la orden con ID %s: %v", id, result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al obtener la orden",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	result = or.db.GetConn().Delete(&ordenGORM)
	if result.Error != nil {
		log.Printf("Error al eliminar la orden: %v", result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al eliminar la orden",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	response, _ := &model.RespuestaEliminacionPedido{
		Mensaje:          "Dirección orden con éxito",
		CodigoEstadoHTTP: http.StatusOK,
	}, result.Error
	return ToJSON(response)
}

func (or *ordenesRepository) Ordenes() (string, error) {
	var ordenesGORM []model.OrdenGORM
	result := or.db.GetConn().Preload("Usuario").Preload("Relaciones.Producto").Find(&ordenesGORM)

	if result.Error != nil {
		log.Printf("Error al obtener las ordenes: %v", result.Error)
		return "Error", result.Error
	}

	ordenes := make([]*model.Orden, 0, len(ordenesGORM))
	for _, ordenGORM := range ordenesGORM {
		orden, _ := ordenGORM.ToGQL()
		ordenes = append(ordenes, orden)
	}

	return ToJSON(ordenes)
}
