package repository

import (
	"fmt"
	"log"
	"net/http"

	"github.com/Zheol/database"
	"github.com/Zheol/model"
	"gorm.io/gorm"
)

type RelacionesRepository interface {
	CrearRelacion(input model.CrearRelacionInput) (string, error)
	Relacion(idProducto string, idOrden string) (string, error)
	ActualizarRelacion(idProducto string, idOrden string, input *model.ActualizarRelacionInput) (string, error)
	EliminarRelacion(idProducto string, idOrden string) (string, error)
	RelacionesDeProducto(idProducto string) (string, error)
	RelacionesDeOrden(idOrden string) (string, error)
	Relaciones() (string, error)
}

type relacionesRepository struct {
	db *database.DB
}

func NewRelacionesRepository(db *database.DB) RelacionesRepository {
	return &relacionesRepository{db}
}

func (rr *relacionesRepository) CrearRelacion(input model.CrearRelacionInput) (string, error) {
	var producto model.ProductoGORM
	var orden model.OrdenGORM

	if err := rr.db.GetConn().First(&producto, input.ProductoID).Error; err != nil {
		return "Error", fmt.Errorf("error al obtener el producto con ID %s: %v", input.ProductoID, err)
	}
	if err := rr.db.GetConn().First(&orden, input.OrdenID).Error; err != nil {
		return "Error", fmt.Errorf("error al obtener la orden con ID %s: %v", input.OrdenID, err)
	}

	relacionGORM := model.RelacionGORM{
		Producto: producto,
		Orden:    orden,
		Canidad:  input.Cantidad,
		Talla:    input.Talla,
		Monto:    input.Monto,
	}

	result := rr.db.GetConn().Create(&relacionGORM)
	if result.Error != nil {
		log.Printf("Error al crear la relacion: %v", result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := relacionGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la relacion de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (rr *relacionesRepository) Relacion(idProducto string, idOrden string) (string, error) {
	var relacionGORM model.RelacionGORM
	result := rr.db.GetConn().Preload("Producto").Preload("Orden.Usuario").Where("producto_id = ? AND orden_id = ?", idProducto, idOrden).First(&relacionGORM)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", result.Error
		}
		log.Printf("Error al obtener la relacion con ID de producto %s y orden %s: %v", idProducto, idOrden, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := relacionGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la relacion de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (rr *relacionesRepository) ActualizarRelacion(idProducto string, idOrden string, input *model.ActualizarRelacionInput) (string, error) {
	var relacionGORM model.RelacionGORM
	result := rr.db.GetConn().Where("producto_id = ? AND orden_id = ?", idProducto, idOrden).First(&relacionGORM)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", result.Error
		}
		log.Printf("Error al obtener la relacion con ID de producto %s y orden %q: %v", idProducto, idOrden, result.Error)
		return "Error", result.Error
	}

	if input.Cantidad != nil {
		relacionGORM.Canidad = *input.Cantidad
	}
	if input.Talla != nil {
		relacionGORM.Talla = *input.Talla
	}
	if input.Monto != nil {
		relacionGORM.Monto = *input.Monto
	}

	result = rr.db.GetConn().Save(&relacionGORM)
	if result.Error != nil {
		log.Printf("Error al actualizar la relacion con IDs %s %q: %v", idProducto, idOrden, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := relacionGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir la relacion de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (rr *relacionesRepository) EliminarRelacion(idProducto string, idOrden string) (string, error) {
	var relacionGORM model.RelacionGORM
	result := rr.db.GetConn().Where("producto_id = ?", idProducto).Where("orden_id = ?", idOrden).First(&relacionGORM)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {

			response, _ := &model.RespuestaEliminacionPedido{
				Mensaje:          "La relacion no existe",
				CodigoEstadoHTTP: http.StatusNotFound,
			}, result.Error
			return ToJSON(response)

		}
		log.Printf("Error al obtener la relacion con IDs %s %x: %v", idProducto, idOrden, result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al obtener la relacion",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	result = rr.db.GetConn().Delete(&relacionGORM)
	if result.Error != nil {
		log.Printf("Error al eliminar la relacion: %v", result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al eliminar la relacion",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}
	response, _ := &model.RespuestaEliminacionPedido{
		Mensaje:          "relacion eliminada con éxito",
		CodigoEstadoHTTP: http.StatusOK,
	}, result.Error
	return ToJSON(response)
}

func (rr *relacionesRepository) RelacionesDeOrden(idOrden string) (string, error) {
	var relacionesGORM []model.RelacionGORM
	result := rr.db.GetConn().Preload("Producto").Preload("Orden.Usuario").Where("orden_id = ?", idOrden).First(&relacionesGORM)

	if result.Error != nil {
		log.Printf("Error al obtener las relaciones: %v", result.Error)
		return "Error", result.Error
	}

	relaciones := make([]*model.RelacionPedido, 0, len(relacionesGORM))
	for _, relacionGORM := range relacionesGORM {
		relacion, _ := relacionGORM.ToGQL()
		relaciones = append(relaciones, relacion)
	}

	return ToJSON(relaciones)
}

func (rr *relacionesRepository) RelacionesDeProducto(idProducto string) (string, error) {
	var relacionesGORM []model.RelacionGORM
	result := rr.db.GetConn().Preload("Producto").Preload("Orden.Usuario").Where("producto_id = ?", idProducto).First(&relacionesGORM)

	if result.Error != nil {
		log.Printf("Error al obtener las relaciones: %v", result.Error)
		return "Error", result.Error
	}

	relaciones := make([]*model.RelacionPedido, 0, len(relacionesGORM))
	for _, relacionGORM := range relacionesGORM {
		relacion, _ := relacionGORM.ToGQL()
		relaciones = append(relaciones, relacion)
	}

	return ToJSON(relaciones)
}

func (rr *relacionesRepository) Relaciones() (string, error) {
	var relacionesGORM []model.RelacionGORM
	result := rr.db.GetConn().Preload("Producto").Preload("Orden.Usuario").Find(&relacionesGORM)

	if result.Error != nil {
		log.Printf("Error al obtener las relaciones: %v", result.Error)
		return "Error", result.Error
	}

	relaciones := make([]*model.RelacionPedido, 0, len(relacionesGORM))
	for _, relacionGORM := range relacionesGORM {
		relacion, _ := relacionGORM.ToGQL()
		relaciones = append(relaciones, relacion)
	}

	return ToJSON(relaciones)
}
