package repository

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/Zheol/database"
	"github.com/Zheol/model"
	"gorm.io/gorm"
)

type ProductosRepository interface {
	CrearProducto(input model.CrearProductoInput) (string, error)
	Producto(id string) (string, error)
	ActualizarProducto(id string, input *model.ActualizarProductoInput) (string, error)
	EliminarProducto(id string) (string, error)
	Productos() (string, error)
}

type productosRepository struct {
	db *database.DB
}

func NewProductoRepository(db *database.DB) ProductosRepository {
	return &productosRepository{db}
}

func (pr *productosRepository) CrearProducto(input model.CrearProductoInput) (string, error) {
	productoGORM := &model.ProductoGORM{
		Nombre: input.Nombre,
	}

	result := pr.db.GetConn().Create(&productoGORM)
	if result.Error != nil {
		log.Printf("Error al crear el producto: %v", result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := productoGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el producto de GORM a GQL: %v", result.Error)
		return "Error", result.Error
	}

	return ToJSON(objetoGQL)
}

func (pr *productosRepository) Producto(id string) (string, error) {
	var productoGORM model.ProductoGORM

	idInt, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error al convertir el ID de string a int: %v", err)
		return "Error", err
	}

	result := pr.db.GetConn().First(&productoGORM, idInt)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", result.Error
		}
		log.Printf("Error al obtener el producto con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := productoGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el producto de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (pr *productosRepository) ActualizarProducto(id string, input *model.ActualizarProductoInput) (string, error) {
	var productoGORM model.ProductoGORM
	result := pr.db.GetConn().First(&productoGORM, id)

	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", fmt.Errorf("Producto con ID %s no encontrado", id)
		}
		log.Printf("Error al buscar el Producto con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	if input.Nombre != nil {
		productoGORM.Nombre = *input.Nombre
	}

	result = pr.db.GetConn().Save(&productoGORM)
	if result.Error != nil {
		log.Printf("Error al actualizar el Producto con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := productoGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el producto de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (pr *productosRepository) EliminarProducto(id string) (string, error) {
	var productoGORM model.ProductoGORM
	result := pr.db.GetConn().First(&productoGORM, id)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {

			response, _ := &model.RespuestaEliminacionPedido{
				Mensaje:          "El producto no existe",
				CodigoEstadoHTTP: http.StatusNotFound,
			}, result.Error
			return ToJSON(response)

		}
		log.Printf("Error al obtener el producto con ID %s: %v", id, result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al obtener el producto",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	result = pr.db.GetConn().Delete(&productoGORM)
	if result.Error != nil {
		log.Printf("Error al eliminar el producto: %v", result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al eliminar el producto",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	response, _ := &model.RespuestaEliminacionPedido{
		Mensaje:          "Producto eliminado con éxito",
		CodigoEstadoHTTP: http.StatusOK,
	}, result.Error
	return ToJSON(response)
}

func (pr *productosRepository) Productos() (string, error) {
	var productosGORM []model.ProductoGORM
	result := pr.db.GetConn().Find(&productosGORM)

	if result.Error != nil {
		log.Printf("Error al obtener los productos: %v", result.Error)
		return "Error", result.Error
	}

	productos := make([]*model.ProductoPedido, 0, len(productosGORM))
	for _, productoGORM := range productosGORM {
		producto, _ := productoGORM.ToGQL()
		productos = append(productos, producto)
	}

	return ToJSON(productos)
}

func ToJSON(obj interface{}) (string, error) {
	jsonData, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	return string(jsonData), err
}
