package repository

import (
	"fmt"
	"log"
	"net/http"

	"github.com/Zheol/database"
	"github.com/Zheol/model"
	"gorm.io/gorm"
)

type UsuariosRepository interface {
	CrearUsuario(input model.CrearUsuarioInput) (string, error)
	Usuario(id string) (string, error)
	ActualizarUsuario(id string, input *model.ActualizarUsuarioInput) (string, error)
	EliminarUsuario(id string) (string, error)
	Usuarios() (string, error)
}

type usuariosRepository struct {
	db *database.DB
}

func NewUsuariosRepository(db *database.DB) UsuariosRepository {
	return &usuariosRepository{db}
}

func (ur *usuariosRepository) CrearUsuario(input model.CrearUsuarioInput) (string, error) {
	usuarioGORM := &model.UsuarioGORM{
		Correo: input.Correo,
	}

	result := ur.db.GetConn().Create(&usuarioGORM)
	if result.Error != nil {
		log.Printf("Error al crear el usuario: %v", result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := usuarioGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el usuario de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (ur *usuariosRepository) Usuario(id string) (string, error) {
	var usuarioGORM model.UsuarioGORM

	result := ur.db.GetConn().Preload("Ordenes.Relaciones.Producto").Where("correo = ?", id).First(&usuarioGORM)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", result.Error
		}
		log.Printf("Error al obtener el usuario con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := usuarioGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el usuario de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (ur *usuariosRepository) ActualizarUsuario(id string, input *model.ActualizarUsuarioInput) (string, error) {
	var usuarioGORM model.UsuarioGORM
	result := ur.db.GetConn().First(&usuarioGORM, id)

	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return "Error", fmt.Errorf("Usuario con ID %s no encontrado", id)
		}
		log.Printf("Error al buscar el usuario con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	// Actualizar los campos proporcionados en el input
	if input.Correo != nil {
		usuarioGORM.Correo = *input.Correo
	}

	// Actualizar el registro en la base de datos
	result = ur.db.GetConn().Save(&usuarioGORM)
	if result.Error != nil {
		log.Printf("Error al actualizar el usuario con ID %s: %v", id, result.Error)
		return "Error", result.Error
	}

	objetoGQL, err := usuarioGORM.ToGQL()
	if err != nil {
		log.Printf("Error al convertir el usuario de GORM a GQL: %v", err)
		return "Error", err
	}

	return ToJSON(objetoGQL)
}

func (ur *usuariosRepository) EliminarUsuario(id string) (string, error) {
	var usuarioGORM model.UsuarioGORM
	result := ur.db.GetConn().First(&usuarioGORM, id)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {

			response, _ := &model.RespuestaEliminacionPedido{
				Mensaje:          "el usuario no existe",
				CodigoEstadoHTTP: http.StatusNotFound,
			}, result.Error
			return ToJSON(response)

		}
		log.Printf("Error al obtener el usuario con ID %s: %v", id, result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al obtener el usuario",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	result = ur.db.GetConn().Delete(&usuarioGORM)
	if result.Error != nil {
		log.Printf("Error al eliminar el usuario: %v", result.Error)
		response, _ := &model.RespuestaEliminacionPedido{
			Mensaje:          "Error al eliminar el usuario",
			CodigoEstadoHTTP: http.StatusInternalServerError,
		}, result.Error
		return ToJSON(response)
	}

	response, _ := &model.RespuestaEliminacionPedido{
		Mensaje:          "Usuario eliminada con éxito",
		CodigoEstadoHTTP: http.StatusOK,
	}, result.Error
	return ToJSON(response)
}

func (ur *usuariosRepository) Usuarios() (string, error) {
	var usuariosGORM []model.UsuarioGORM
	result := ur.db.GetConn().Preload("Ordenes.Relaciones.Producto").Find(&usuariosGORM)

	if result.Error != nil {
		log.Printf("Error al obtener los usuarios: %v", result.Error)
		return "Error", result.Error
	}

	usuarios := make([]*model.UsuarioPedido, 0, len(usuariosGORM))
	for _, usuarioGORM := range usuariosGORM {
		direccion, _ := usuarioGORM.ToGQL()
		usuarios = append(usuarios, direccion)
	}

	return ToJSON(usuarios)
}
