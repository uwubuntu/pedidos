package main

import (
	"encoding/json"
	"fmt"

	"github.com/Zheol/database"
	"github.com/Zheol/model"
	"github.com/Zheol/repository"
	"github.com/streadway/amqp"
)

type Mensaje struct {
	Pattern string          `json:"pattern"`
	Data    json.RawMessage `json:"data"`
	Id      string          `json:"id"`
}

func publish(ch *amqp.Channel, d amqp.Delivery, respuesta string) {
	err := ch.Publish(
		"",
		d.ReplyTo,
		false,
		false,
		amqp.Publishing{
			ContentType:   "application/json",
			CorrelationId: d.CorrelationId,
			Body:          []byte(respuesta),
		},
	)
	if err != nil {
		fmt.Errorf("error al publicar la respuesta: %v", err)
	}
}

func main() {

	db := database.Connect()
	database.EjecutarMigraciones(db.GetConn())
	var ordenRepository = repository.NewOrdenesRepository(db)
	var productoRepository = repository.NewProductoRepository(db)
	var relacionRepository = repository.NewRelacionesRepository(db)
	var usuarioRepository = repository.NewUsuariosRepository(db)

	/*  conn, err := amqp.Dial("amqps://iyyotkip:izxAYzz6ekTnCe7zC3DKbJc2DYcJv4H6@beaver.rmq.cloudamqp.com/iyyotkip") //Conexion rabbitmq en la nube */
	conn, err := amqp.Dial("amqp://guest:guest@172.16.238.5")
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer ch.Close()

	msgs, err := ch.Consume(
		"ms_pedidos_queue",
		"MS_pedidos",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			fmt.Printf("Mensaje recibido: %s\n", d.Body)

			var mensaje Mensaje

			err := json.Unmarshal(d.Body, &mensaje)
			if err != nil {
				fmt.Println("Error al decodificar JSON:", err)
				publish(ch, d, "Error en decodificación")
				continue
			}

			fmt.Printf("pattern: %s\n", mensaje.Pattern)
			var respuesta string

			switch pattern := mensaje.Pattern; pattern {

			////////// USUARIOS //////////

			case "all_users":
				respuesta, err = usuarioRepository.Usuarios()
			case "find_user":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = usuarioRepository.Usuario(intermeditate.ID)
			case "create_user":
				var input model.CrearUsuarioInput
				err = json.Unmarshal([]byte(mensaje.Data), &input)
				fmt.Printf("input: %v\n", input)
				if err == nil {
					respuesta, err = usuarioRepository.CrearUsuario(input)
				}
			case "update_user":
				var intermeditate struct {
					ID    string                       `json:"id"`
					Input model.ActualizarUsuarioInput `json:"input"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				if err == nil {
					respuesta, err = usuarioRepository.ActualizarUsuario(intermeditate.ID, &intermeditate.Input)
				}
			case "delete_user":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = usuarioRepository.EliminarUsuario(intermeditate.ID)

			////////// ORDENES //////////

			case "all_orders":
				respuesta, err = ordenRepository.Ordenes()
			case "find_order":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}

				respuesta, err = ordenRepository.Orden(intermeditate.ID)
			case "create_order":
				var input model.CrearOrdenInput
				err = json.Unmarshal([]byte(mensaje.Data), &input)
				fmt.Printf("input: %v\n", input)
				if err == nil {
					respuesta, err = ordenRepository.CrearOrden(input)
				}
			case "update_order":
				var intermeditate struct {
					ID    string                     `json:"id"`
					Input model.ActualizarOrdenInput `json:"input"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err == nil {
					respuesta, err = ordenRepository.ActualizarOrden(intermeditate.ID, &intermeditate.Input)
				}
			case "delete_order":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = ordenRepository.EliminarOrden(intermeditate.ID)

			////////// PRODUCTOS //////////

			case "all_products":
				respuesta, err = productoRepository.Productos()
			case "find_product":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = productoRepository.Producto(intermeditate.ID)
			case "create_product":
				var input model.CrearProductoInput
				err = json.Unmarshal([]byte(mensaje.Data), &input)
				fmt.Printf("input: %v\n", input)
				if err == nil {
					respuesta, err = productoRepository.CrearProducto(input)
				}
			case "update_product":
				var intermeditate struct {
					ID    string                        `json:"id"`
					Input model.ActualizarProductoInput `json:"input"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err == nil {
					respuesta, err = productoRepository.ActualizarProducto(intermeditate.ID, &intermeditate.Input)
				}
			case "delete_product":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = productoRepository.EliminarProducto(intermeditate.ID)

			////////// RELACIONES //////////

			case "all_relations":
				respuesta, err = relacionRepository.Relaciones()
			case "find_relation":
				var intermeditate struct {
					IdProducto string `json:"idproducto"`
					IdOrden    string `json:"idorden"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err == nil {
					respuesta, err = relacionRepository.Relacion(intermeditate.IdProducto, intermeditate.IdOrden)
				}

			case "create_relation":
				var input model.CrearRelacionInput
				err = json.Unmarshal([]byte(mensaje.Data), &input)
				fmt.Printf("input: %v\n", input)
				if err == nil {
					respuesta, err = relacionRepository.CrearRelacion(input)
				}
			case "update_relation":
				var intermeditate struct {
					IdProducto string                        `json:"idproducto"`
					IdOrden    string                        `json:"idorden"`
					Input      model.ActualizarRelacionInput `json:"input"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err == nil {
					respuesta, err = relacionRepository.ActualizarRelacion(intermeditate.IdProducto, intermeditate.IdOrden, &intermeditate.Input)
				}
			case "delete_relation":
				var intermeditate struct {
					IdProducto string `json:"idproducto"`
					IdOrden    string `json:"idorden"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err == nil {
					respuesta, err = relacionRepository.EliminarRelacion(intermeditate.IdProducto, intermeditate.IdOrden)
				}
			case "find_relation_Order":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = relacionRepository.RelacionesDeOrden(intermeditate.ID)

			case "find_relation_Product":
				var intermeditate struct {
					ID string `json:"id"`
				}
				err = json.Unmarshal([]byte(mensaje.Data), &intermeditate)
				if err != nil {
					fmt.Println("Error al decodificar JSON:", err)
					publish(ch, d, "Error")
				}
				respuesta, err = relacionRepository.RelacionesDeProducto(intermeditate.ID)

			////////// DEFAULT //////////

			default:
				err = fmt.Errorf("patrón no reconocido: %s", mensaje.Pattern)
			}
			if err != nil {
				fmt.Println("Error al procesar el mensaje:", err)
				publish(ch, d, "Error al procesar la solicitud")
				continue
			}

			publish(ch, d, respuesta)
			d.Ack(false)
		}
	}()

	<-forever
}
